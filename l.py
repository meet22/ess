def main(n):
    rec_fibo(1,1,0,n)

def rec_fibo(prev1,prev2,curr_no,n):
    if(curr_no<n):     
        if ((prev1+prev2)%15==0):
            print "FizzBuzz"
            rec_fibo(prev2,(prev1+prev2),curr_no+1,n)
        elif ((prev1+prev2)%3==0):
            print "Buzz"
            rec_fibo(prev2,(prev1+prev2),curr_no+1,n)
        elif ((prev1+prev2)%5==0):
            print "Fizz"
            rec_fibo(prev2,(prev1+prev2),curr_no+1,n)
        else:
            flag=0
            for num in range(2,(prev1+prev2)):
                if((prev1+prev2)%num):
                    continue
                else:
                    flag=1
                    break
            if(flag):
                print str((prev1+prev2))
                rec_fibo(prev2,(prev1+prev2),curr_no+1,n)
            else:
                print "BuzzFizz"
                rec_fibo(prev2,(prev1+prev2),curr_no+1,n)
    else:
        return

main(5)
